﻿using System;
using System.IO;
using SQLite;
using Xamarin.Forms;
using one4all.Droid;
using SQLite.Net;
using SQLite.Net.Platform.XamarinAndroid;

[assembly: Dependency(typeof(SQLiteDb))]

namespace one4all.Droid
{
    public class SQLiteDb : ISQLiteDb
    {
        public SQLiteConnection GetConnection()
        {
            var documentsPath = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
            var path = Path.Combine(documentsPath, "MySQLite2.db3");
            return new SQLiteConnection(new SQLitePlatformAndroid(), path);
        }
    }
}
