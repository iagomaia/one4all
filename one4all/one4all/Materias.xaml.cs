﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using one4all.Models;
using System.Collections.ObjectModel;
using SQLite.Net;
using SQLiteNetExtensions.Extensions;

namespace one4all
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class Materias : ContentPage
	{
        private Usuario _user;
        private SQLiteConnection _connection;
        private ObservableCollection<Materia> _materias;
		public Materias (ref Usuario u)
		{
            _user = u ?? throw new ArgumentNullException();
			InitializeComponent ();

        }

        async protected override void OnAppearing()
        {
            _connection = DependencyService.Get<ISQLiteDb>().GetConnection();
            try
            {
                List<Materia> todas = _connection.Table<Materia>().ToList();
                List<Materia> inscrito = _user.Materias;
                todas.RemoveAll(m => inscrito.Any(mi => mi.Id==m.Id));
                _materias = new ObservableCollection<Materia>(todas);
                MateriaList.ItemsSource = _materias;
            }
            catch (Exception ex)
            {
                await DisplayAlert("Erro", ex.ToString(), "OK");
            }
            base.OnAppearing();
        }

        async private void Inscrever_Activated(object sender, EventArgs e)
        {
            try
            {
                if (MateriaList.SelectedItem != null)
                {
                    Materia m = MateriaList.SelectedItem as Materia;
                    _user.Materias.Add(m);
                    _connection.UpdateWithChildren(m);

                    await DisplayAlert("Inscrito!", "Inscrição realizada com sucesso!", "OK");
                    _materias.Remove(MateriaList.SelectedItem as Materia);
                }
                else
                {
                    await DisplayAlert("Erro!", "Selecione uma matéria!", "OK");
                }

            } catch (Exception ex)
            {
                await DisplayAlert("Erro", ex.ToString(), "OK");
            }
        }
    }
}