﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using one4all.Models;
using System.Collections.ObjectModel;
using SQLite.Net;
using SQLiteNetExtensions.Extensions;

namespace one4all
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class AddThreadForm : ContentPage
    {
        private Usuario _user;
        private Materia _materia;
        private SQLiteConnection _connection;
        private bool _flag;

        public AddThreadForm(ref Materia m, ref Usuario u)
        {
            _user = u ?? throw new ArgumentNullException();
            _materia = m ?? throw new ArgumentNullException();
            InitializeComponent();
        }

        protected override void OnAppearing()
        {
            _connection = DependencyService.Get<ISQLiteDb>().GetConnection();
            base.OnAppearing();
        }

        async private Task VerificaTitulo()
        {
            if (TituloEntry.Text == null)
            {
                _flag = false;
                await DisplayAlert("Erro", "O campo título não pode estar vazio!", "OK");
            }
        }

        async private Task VerificaConteudo()
        {
            if (ConteudoEditor.Text == null)
            {
                _flag = false;
                await DisplayAlert("Erro", "O campo de descrição não pode estar vazio!", "OK");
            }
        }

        async private void EnviarBtn_Clicked(object sender, EventArgs e)
        {
            _flag = true;
            await VerificaTitulo();
            await VerificaConteudo();
            if (_flag)
            {
                ForumThread t = new ForumThread(_materia, _user, TituloEntry.Text, ConteudoEditor.Text);
                try
                {
                    _connection.Insert(t);
                    _materia.Threads.Add(t);
                    _user.Threads.Add(t);
                    _connection.UpdateWithChildren(_materia);
                    _connection.UpdateWithChildren(_user);
                    await DisplayAlert("Sucesso!", "Tópico criado com sucesso!", "OK");
                    await Navigation.PopModalAsync();
                }
                catch (Exception ex)
                {
                    await DisplayAlert("Erro", ex.ToString(), "OK");
                }
            }
            

        }

        async private void VoltarBtn_Clicked(object sender, EventArgs e)
        {
            bool voltar = await DisplayAlert("Sair?", "Deseja sair sem salvar as alterações?", "Sair", "Cancelar");
            if (voltar) await Navigation.PopModalAsync();
        }
    }
}