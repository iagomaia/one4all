﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using one4all.Models;
using SQLite;
using SQLite.Net;
using SQLiteNetExtensions.Extensions;

namespace one4all
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ModeradorEditEvento : ContentPage
    {
        private Usuario _user;
        private Materia _materia;
        private Evento _evento;
        private SQLiteConnection _connection;

        public ModeradorEditEvento(Usuario u, Materia m, Evento e)
        {
            _user = u ?? throw new ArgumentNullException();
            _materia = m ?? throw new ArgumentNullException();
            _evento = e ?? throw new ArgumentNullException();
            InitializeComponent();
            BindingContext = _evento;
        }

        protected override void OnAppearing()
        {
            _connection = DependencyService.Get<ISQLiteDb>().GetConnection();
            base.OnAppearing();
        }

        async private void ExcluirBtn_Clicked(object sender, EventArgs e)
        {
            if(await DisplayAlert("Excluir?", "Tem certeza que deseja excluir o item selecionado?", "Sim", "Não"))
            {
                try
                {
                    _connection.Delete(_evento, true);
                    await DisplayAlert("Excluído", "O evento foi excluido com sucesso!", "OK");
                    await Navigation.PopModalAsync();
                }catch(Exception ex)
                {
                    await DisplayAlert("Erro", ex.ToString(), "OK");
                }
            }
        }

        protected override bool OnBackButtonPressed()
        {
            return true;
        }

        async private void VoltarBtn_Clicked(object sender, EventArgs e)
        {
            bool voltar = await DisplayAlert("Sair?", "Deseja sair sem salvar as alterações?", "Sair", "Cancelar");
            if (voltar) await Navigation.PopModalAsync();
        }

        async private void EnviarBtn_Clicked(object sender, EventArgs e)
        {
            Evento ev = _evento;
            ev.Nome = NomeEvento.Text;
            ev.Descricao = DescEvento.Text;
            ev.Valor = Convert.ToInt32(ValorEvento.Text);
            ev.Data = new DateTime(DataEvento.Date.Year, DataEvento.Date.Month, DataEvento.Date.Day);
            try
            {
                _connection.InsertOrReplace(ev);
                _connection.UpdateWithChildren(_materia);
                await DisplayAlert("Sucesso", "Alterações realizadas com sucesso!", "OK");
                await Navigation.PopModalAsync();
            }catch(Exception ex)
            {
                await DisplayAlert("Erro", ex.ToString(), "OK");
            }

        }
    }
}