﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using one4all.Models;
using SQLite;
using SQLite.Net;

namespace one4all
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ModeradorAddMateria : ContentPage
    {
        private SQLiteConnection _connection;
        public ModeradorAddMateria()
        {
            InitializeComponent();
        }

        protected override bool OnBackButtonPressed()
        {
            return true;
        }

        protected override void OnAppearing()
        {
            _connection = DependencyService.Get<ISQLiteDb>().GetConnection();
            base.OnAppearing();
        }

        async private void VoltarBtn_Clicked(object sender, EventArgs e)
        {
            bool voltar = await DisplayAlert("Sair?", "Deseja sair sem salvar as alterações?", "Sair", "Cancelar");

            if (voltar) await Navigation.PopModalAsync();
        }

        async private void AdicionarBtn_Clicked(object sender, EventArgs e)
        {
            Materia m = new Materia(nome.Text, professor.Text, Convert.ToInt32(periodo.Text));
            try
            {
                _connection.Insert(m);
                if (await DisplayAlert(m.Nome, "Cadastro realizado com sucesso!", "Sair", "Add outra"))
                    await Navigation.PopModalAsync();
                else{
                    nome.Text = "";
                    professor.Text = "";
                    periodo.Text = "";
                }

            } catch(Exception ex)
            {
                await DisplayAlert("Erro", ex.ToString(), "OK");
            }

        }
    }

}