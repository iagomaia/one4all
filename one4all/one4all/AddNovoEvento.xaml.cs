﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using one4all.Models;
using SQLite.Net;
using SQLiteNetExtensions.Extensions;
using System.Collections.ObjectModel;

namespace one4all
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class AddNovoEvento : ContentPage
    {
        private SQLiteConnection _connection;
        private Usuario _user;
        private Materia _materia;
        private List<Materia> _materias;
        private bool _flag;
        ObservableCollection<Evento> _eventos;

        public AddNovoEvento(ref Usuario u, ref ObservableCollection<Evento> evs)
        {
            _user = u ?? throw new ArgumentNullException();
            _eventos = evs ?? throw new ArgumentNullException();
            InitializeComponent();
        }

        protected override void OnAppearing()
        {
            _connection = DependencyService.Get<ISQLiteDb>().GetConnection();
            _materias = _user.Materias;
            foreach (Materia m in _materias)
                MateriaPicker.Items.Add(m.Nome);
                base.OnAppearing();
        }

        protected override bool OnBackButtonPressed()
        {
            return true;
        }

        async private void VoltarBtn_Clicked(object sender, EventArgs e)
        {
            bool voltar = await DisplayAlert("Sair?", "Deseja sair sem salvar as alterações?", "Sair", "Cancelar");
            if (voltar) await Navigation.PopModalAsync();
        }

        private void MateriaPicker_SelectedIndexChanged(object sender, EventArgs e)
        {
            var nome = MateriaPicker.Items[MateriaPicker.SelectedIndex];
            _materia = _materias.Single(m => m.Nome.Equals(nome));
        }

        async private Task VerificaNome()
        {
            if (NomeEvento.Text == null)
            {
                _flag = false;
                await DisplayAlert("Erro", "O campo nome não pode estar vazio!", "OK");
            }
        }

        async private Task VerificaDesc()
        {
            if (DescEvento.Text == null){
                _flag = false;
                await DisplayAlert("Erro", "O campo de descrição não pode estar vazio!", "OK");
            }
        }

        async private Task VerificaValor()
        {
            if (ValorEvento.Text == null)
            {
                _flag = false;
                await DisplayAlert("Erro", "O campo de valor não pode estar vazio!", "OK");
            }
        }

        async private void EnviarBtn_Clicked(object sender, EventArgs e)
        {
            _flag = true;
            await VerificaNome();
            await VerificaDesc();
            await VerificaValor();
            if (_flag)
            {
                Evento ev = new Evento(
                    new DateTime(DataEvento.Date.Year, DataEvento.Date.Month, DataEvento.Date.Day),
                    NomeEvento.Text, DescEvento.Text, Convert.ToInt32(ValorEvento.Text), _materia, _user);
                try
                {
                    _connection.Insert(ev);
                    _eventos.Add(ev);
                    _materia.Eventos.Add(ev);
                    _connection.UpdateWithChildren(_materia);
                    await DisplayAlert("Sucesso!", "Evento criado com sucesso!", "OK");
                    await Navigation.PopModalAsync();
                }
                catch (Exception ex)
                {
                    await DisplayAlert("Erro", ex.ToString(), "OK");
                }
            }
        }
    }
}