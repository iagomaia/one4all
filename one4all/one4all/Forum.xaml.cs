﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using one4all.Models;
using System.Collections.ObjectModel;
using SQLite.Net;
using SQLiteNetExtensions.Extensions;

namespace one4all
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Forum : ContentPage
    {
        private Usuario _user;
        private SQLiteConnection _connection;
        private ObservableCollection<Materia> _materias;
        public Forum(ref Usuario u)
        {
            _user = u ?? throw new ArgumentNullException();
            InitializeComponent();
        }

        async protected override void OnAppearing()
        {
            _connection = DependencyService.Get<ISQLiteDb>().GetConnection();
            _materias = new ObservableCollection<Materia>(_user.Materias);
            MateriaList.ItemsSource = _materias;
            base.OnAppearing();
        }

        async private void MateriaList_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            if (e.SelectedItem == null) return;
            var m = e.SelectedItem as Materia;
            await Navigation.PushAsync(new ThreadsForum(ref _user, m));
            MateriaList.SelectedItem = null;
        }
    }
}