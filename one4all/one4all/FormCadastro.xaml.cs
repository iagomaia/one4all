﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using SQLite;
using one4all.Models;
using SQLite.Net;

namespace one4all
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class FormCadastro : ContentPage
    {
        private SQLiteConnection _connection;
        private bool _flag = true;

        public FormCadastro()
        {
            InitializeComponent();
        }

        async protected override void OnAppearing()
        {
            _connection =  DependencyService.Get<ISQLiteDb>().GetConnection();
            base.OnAppearing();
        }

        async private void VoltarBtn_Clicked(object sender, EventArgs e)
        {
            await Navigation.PopModalAsync();
        }

        async private Task VerificaNome()
        {
            try
            { 
                if (nome.Text == null)
                {
                    _flag = false;
                    await DisplayAlert("Erro", "O campo nome não pode estar vazio!", "OK");
                }
            } catch (Exception ex)
            {
                await DisplayAlert("Erro", ex.ToString(), "OK");
            }
        }

        async private Task VerificaEmail()
        {
            if (email.Text == null || !email.Text.Contains("@") || !email.Text.Contains(".com"))
            {
                _flag = false;
                await DisplayAlert("Erro", "Verifique o campo email!", "OK");
            }
        }

        async private Task VerificaPeriodo()
        {
            if (periodo.Text == null)
            {
                _flag = false;
                await DisplayAlert("Erro", "Verifique o campo período!", "OK");
            }
        }

        async private Task VerificaMatricula()
        {
            if (matricula.Text == null)
            {
                _flag = false;
                await DisplayAlert("Erro", "Verifique o campo de matrícula!", "OK");
            }
        }

        async private Task VerificaSenha()
        {
            if (senha.Text != confirmaSenha.Text)
            {
                _flag = false;
                await DisplayAlert("Erro", "As senhas não conferem!", "OK");
            }
            if (senha.Text == null)
            {
                _flag = false;
                await DisplayAlert("Erro", "A senha não pode estar em branco!", "OK");
            }
        }

        async private void CadastrarBtn_Clicked(object sender, EventArgs e)
        {
            try
            {
                _flag = true;
                await VerificaNome();
                await VerificaMatricula();
                await VerificaPeriodo();
                await VerificaEmail();
                await VerificaSenha();
                if (_flag)
                {
                    String n = nome.Text;
                    String m = matricula.Text;
                    int p = 0;
                    if (periodo.Text != "")
                        p = Convert.ToInt32(periodo.Text);
                    String em = email.Text;
                    String s = senha.Text;
                    Usuario user = new Usuario(n, p, m, em, s);
                    if (_flag)
                    {
                        _connection.Insert(user);
                        await DisplayAlert("Cadastrado!", "Cadastro realizado com sucesso!", "OK");
                        await Navigation.PopModalAsync();
                    }
                }
            }
            catch (Exception ex)
            {
                await DisplayAlert("Erro", "Houve um erro durante o cadastro, tente novamente", "OK");
            }
        }
    }
}