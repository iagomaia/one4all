﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using one4all.Models;
using SQLite;
using SQLite.Net;

namespace one4all
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Config : ContentPage
    {
        private Usuario _user;
        private SQLiteConnection _connection;
        private bool _flag;

        public Config(Usuario u)
        {
            _user = u ?? throw new ArgumentNullException();
            InitializeComponent();
            BindingContext = _user;
        }

        protected override void OnAppearing()
        {
            _connection = DependencyService.Get<ISQLiteDb>().GetConnection();
            base.OnAppearing();
        }

        async private void VoltarBtn_Clicked(object sender, EventArgs e)
        {
            bool voltar = await DisplayAlert("Descartar Alterações?", "Deseja sair sem salvar as alterações?", "Sair", "Cancelar");
            if (voltar) await Navigation.PopModalAsync();
        }

        async private Task ValidaNome()
        {
            if (nome.Text == null)
            {
                _flag = false;
                await DisplayAlert("Erro", "O campo de nome não pode estar vazio!", "OK");
            }
        }

        async private Task ValidaPeriodo()
        {
            if (periodo.Text == null)
            {
                _flag = false;
                await DisplayAlert("Erro", "O campo de período não pode estar vazio!", "OK");
            }
        }

        async private Task ValidaSenha()
        {
            if (senha.Text != _user.Senha)
            {
                _flag = false;
                await DisplayAlert("Erro", "Verifique se a senha antiga está correta!", "OK");
                return;
            }
            if (novaSenha.Text != confirmaSenha.Text && novaSenha.Text != null)
            {
                _flag = false;
                await DisplayAlert("Erro", "As senhas não conferem!", "OK");
                return;
            }
        }

        async private void SalvarBtn_Clicked(object sender, EventArgs e)
        {
            _flag = true;
            await ValidaNome();
            await ValidaPeriodo();
            await ValidaSenha();
            if (_flag)
            {
                Usuario u = _user;
                u.Nome = nome.Text;
                u.Matricula = matricula.Text;
                u.Periodo = Convert.ToInt32(periodo.Text);
                if (novaSenha.Text != null) u.Senha = novaSenha.Text;
                try
                {
                    _connection.InsertOrReplace(_user);
                    await DisplayAlert("Sucesso!", "Dados alterados com sucesso!", "OK");
                    await Navigation.PopModalAsync();
                } catch (Exception ex)
                {
                    await DisplayAlert("Erro", ex.ToString(), "OK");
                }
            }
        }
    }
}