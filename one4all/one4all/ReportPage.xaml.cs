﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace one4all
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ReportPage : ContentPage
    {
        public ReportPage(Models.Evento evento, Models.ForumThread thread)
        {
            InitializeComponent();
        }

        private void EnviarBtn_Clicked(object sender, EventArgs e)
        {

        }

        protected override bool OnBackButtonPressed()
        {
            return true;
        }
        
        async private void VoltarBtn_Clicked(object sender, EventArgs e)
        {
            if (await DisplayAlert("Sair?", "Deseja sair sem enviar seu report?", "Sair", "Cancelar"))
                 await Navigation.PopModalAsync();
        }
    }
}