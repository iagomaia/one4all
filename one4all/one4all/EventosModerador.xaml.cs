﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using one4all.Models;
using SQLite;
using System.Collections.ObjectModel;
using SQLite.Net;
using SQLiteNetExtensions.Extensions;

namespace one4all
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class EventosModerador : ContentPage
    {
        private SQLiteConnection _connection;
        private Usuario _user;
        private Materia _materia;
        public EventosModerador(Usuario u, ref Materia m)
        {
            _user = u ?? throw new ArgumentNullException();
            _materia = m ?? throw new ArgumentNullException();
            InitializeComponent();
        }

        async protected override void OnAppearing()
        {
            _connection = DependencyService.Get<ISQLiteDb>().GetConnection();

            try
            {
                //_materia.Eventos = _connection.Table<Evento>().Where(e => e.IdMateria.Equals(_materia.Id)).ToList();
                _materia = _connection.GetWithChildren<Materia>(_materia.Id);
                EventosList.ItemsSource = new ObservableCollection<Evento>(_materia.Eventos);
            } catch (Exception ex)
            {
                await DisplayAlert("Erro", ex.ToString(), "OK");
            }
            base.OnAppearing();
        }

        async private void EditEvento_Activated(object sender, EventArgs e)
        {
            if (EventosList.SelectedItem != null)
            {
                await Navigation.PushModalAsync(new ModeradorEditEvento(_user, _materia, EventosList.SelectedItem as Evento));
                _materia = _connection.GetWithChildren<Materia>(_materia.Id);
                EventosList.ItemsSource = new ObservableCollection<Evento>(_materia.Eventos);
            }
        }

        async private void AddEvento_Activated(object sender, EventArgs e)
        {
            await Navigation.PushModalAsync(new AddEventoModerador(_user, _materia));
            _materia = _connection.GetWithChildren<Materia>(_materia.Id);
            EventosList.ItemsSource = new ObservableCollection<Evento>(_materia.Eventos);
        }
    }
}