﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using SQLite;
using one4all.Models;
using System.Collections.ObjectModel;
using SQLite.Net;

namespace one4all
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ThreadsModerador : ContentPage
    {
        private Usuario _user;
        private Materia _materia;
        private List<ForumThread> _threads;
        private SQLiteConnection _connection;

        public ThreadsModerador(Usuario u, Materia m)
        {
            _user = u;
            _materia = m;
            InitializeComponent();
        }

        async protected override void OnAppearing()
        {
            _connection = DependencyService.Get<ISQLiteDb>().GetConnection();
            try
            {
                _materia.Threads = _connection.Table<ForumThread>().Where(ft => ft.IdMateria.Equals(_materia.Id)).ToList();
                ObservableCollection<ForumThread> threadList = new ObservableCollection<ForumThread>(_materia.Threads);
                ThreadsList.ItemsSource = threadList;

            } catch (Exception ex)
            {
                await DisplayAlert("Erro", ex.ToString(), "OK");
            }
            base.OnAppearing();
        }

        async private void AddThread_Activated(object sender, EventArgs e)
        {
            await Navigation.PushModalAsync(new ModeradorAddThread(_materia, _user));
        }

        async private void EditThread_Activated(object sender, EventArgs e)
        {
            await Navigation.PushModalAsync(new ModeradorEditThread(ThreadsList.SelectedItem as ForumThread));
        }

        async private void Coments_Activated(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new ComentModerador(_user, ThreadsList.SelectedItem as ForumThread));
        }
    }
}