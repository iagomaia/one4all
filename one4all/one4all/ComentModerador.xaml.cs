﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using SQLite;
using one4all.Models;
using System.Collections.ObjectModel;
using SQLite.Net;

namespace one4all
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ComentModerador : ContentPage
    {
        private Usuario _user;
        private ForumThread _thread;
        private SQLiteConnection _connection;
        public ComentModerador(Usuario u, ForumThread ft)
        {
            _thread = ft ?? throw new ArgumentNullException();
            _user = u ?? throw new ArgumentNullException();
            InitializeComponent();
        }

        async protected override void OnAppearing()
        {
            _connection = DependencyService.Get<ISQLiteDb>().GetConnection();
            _thread.Comentarios = _connection.Table<Comentario>().Where(c => c.IdThread.Equals(_thread.Id)).ToList();
            ObservableCollection<Comentario> cs = new ObservableCollection<Comentario>(_thread.Comentarios);
            ComentsList.ItemsSource = cs;
            base.OnAppearing();
        }

        async private void ExcluirComent_Activated(object sender, EventArgs e)
        {
            try
            {
                bool flag = await DisplayAlert("Tem certeza?", "Deseja realmente excluir o comentário selecionado?", "Sim", "Não");
                if (flag)
                {
                   _connection.Delete(ComentsList.SelectedItem as Comentario);
                }
            }catch(Exception ex)
            {
                await DisplayAlert("Erro", ex.ToString(), "OK");
            }
        }

        async private void AddComent_Activated(object sender, EventArgs e)
        {
            await Navigation.PushModalAsync(new ModeradorAddComentario(_user, _thread));
        }
    }
}