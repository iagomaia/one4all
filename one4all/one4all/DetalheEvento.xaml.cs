﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace one4all
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class DetalheEvento : ContentPage
	{
        public Models.Evento ev { get; set; }
		public DetalheEvento (Models.Evento evento)
		{
            BindingContext = evento ?? throw new ArgumentNullException();
            ev = evento;
			InitializeComponent ();
		}
    }
}