﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace one4all.Models
{
    public class Report
    {
        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }
        [MaxLength(1000)]
        public String Texto { get; set; }
        public Usuario User { get; set; }
        public Usuario Destinatario { get; set; }
        public Evento EventoReportado { get; set; }
        public ForumThread ThreadReportada { get; set; }
        [MaxLength(10)]
        public String Tipo { get; set; }

        public Report()
        {

        }

        public Report(String texto, Usuario user, Usuario dest, Evento evento)
        {
            this.Texto = texto;
            this.User = user;
            this.Destinatario = dest;
            this.EventoReportado = evento;
            this.ThreadReportada = null;
            this.Tipo = "Evento";
        }

        public Report(String texto, Usuario user, Usuario dest, ForumThread thread)
        {
            this.Texto = texto;
            this.User = user;
            this.Destinatario = dest;
            this.EventoReportado = null;
            this.ThreadReportada = thread;
            this.Tipo = "Tópico";
        }
    }
}
