﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SQLite;
using SQLiteNetExtensions.Attributes;
using SQLite.Net.Attributes;

namespace one4all.Models
{
    public class Evento
    {
        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }
        public DateTime Data { get; set; }
        [MaxLength(50)]
        public String Nome { get; set; }
        [MaxLength(255)]
        public String Descricao { get; set; }
        public int Valor { get; set; }
        [ForeignKey(typeof(Materia))]
        public int IdMateria { get; set; }
        [ForeignKey(typeof(Usuario))]
        public int IdAutor { get; set; }

        [ManyToOne]
        public Materia Materia { get; set; }
        [ManyToOne]
        public Usuario Autor { get; set; }

        public Evento()
        {

        }

        public Evento(DateTime data, String nome, String desc, int valor, Materia materia, Usuario autor)
        {
            this.Data = data;
            this.IdMateria = materia.Id;
            this.Nome = nome;
            this.Descricao = desc;
            this.Valor = valor;
            this.IdAutor = autor.Id;
        }

        public Evento(int i, DateTime data, String nome, String desc, int valor, Materia materia, Usuario autor)
        {
            this.Id = i;
            this.Data = data;
            this.IdMateria = materia.Id;
            this.Nome = nome;
            this.Descricao = desc;
            this.Valor = valor;
            this.IdAutor = autor.Id;
        }
    }
}
