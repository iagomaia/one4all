﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SQLiteNetExtensions.Attributes;
using SQLite.Net.Attributes;

namespace one4all.Models
{
    public class Comentario
    {
        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }
        [ForeignKey(typeof(Usuario))]
        public int IdAutor { get; set; }
        [ForeignKey(typeof(ForumThread))]
        public int IdThread { get; set; }
        [MaxLength(1000)]
        public String Texto { get; set; }

        [ManyToOne]
        public Usuario Autor { get; set; }
        [ManyToOne]
        public ForumThread Thread { get; set; }
        public Comentario()
        {

        }

        public Comentario(Usuario u, String txt, ForumThread t)
        {
            this.IdAutor = u.Id;
            this.Texto = txt;
            this.IdThread = t.Id;
        }

        public Comentario(int i, Usuario u, String txt, ForumThread t)
        {
            this.Id = i;
            this.IdAutor = u.Id;
            this.Texto = txt;
            this.IdThread = t.Id;
        }
    }
}
