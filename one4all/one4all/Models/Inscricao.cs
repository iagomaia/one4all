﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SQLite;
using SQLiteNetExtensions.Attributes;

namespace one4all.Models
{
    public class Inscricao
    {
        [ForeignKey(typeof(Usuario))]
        public int IdAluno { get; set; }
        [ForeignKey(typeof(Materia))]
        public int IdMateria { get; set; }

        public Inscricao()
        {

        }
    }

    
}
