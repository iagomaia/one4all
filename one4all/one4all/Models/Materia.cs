﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SQLiteNetExtensions.Attributes;
using SQLite.Net.Attributes;

namespace one4all.Models
{
    public class Materia
    {
        [PrimaryKey, AutoIncrement]
        public int Id { set; get; }
        [MaxLength(50)]
        public String Nome { set; get; }
        [MaxLength(150)]
        public String Professor { set; get; }
        public int Periodo { get; set; }

        [OneToMany(CascadeOperations = CascadeOperation.All)]
        public List<Evento> Eventos { get; set; }

        [OneToMany(CascadeOperations = CascadeOperation.All)]
        public List<ForumThread> Threads { get; set; }

        [ManyToMany(typeof(Inscricao))]
        public List<Usuario> Alunos { get; set; }

        public Materia()
        {

        }

        public Materia(String n, String p, int pd)
        {
            this.Nome = n;
            this.Professor = p;
            this.Periodo = pd;
        }

        public Materia (int i, String n, String p, int pd)
        {
            this.Id = i;
            this.Nome = n;
            this.Professor = p;
            this.Periodo = pd;
        }
    }
}
