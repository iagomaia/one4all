﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SQLiteNetExtensions.Attributes;
using SQLite.Net.Attributes;

namespace one4all.Models
{
    public class ForumThread
    {
        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }
        [ForeignKey(typeof(Materia))]
        public int IdMateria { get; set; }
        [ForeignKey(typeof(Usuario))]
        public int IdUsuario { get; set; }
        [MaxLength(140)]
        public String Titulo { get; set; }
        [MaxLength(255)]
        public String Descricao { get; set; }

        [ManyToOne]
        public Usuario OriginalPoster { get; set; }

        [ManyToOne]
        public Materia Materia { get; set; }

        [OneToMany(CascadeOperations = CascadeOperation.All)]
        public List<Comentario> Comentarios { get; set; }

        public ForumThread()
        {
        }

        public ForumThread(Materia m, Usuario op, String t, String d)
        {
            this.IdMateria = m.Id;
            this.IdUsuario = op.Id;
            this.Titulo = t;
            this.Descricao = d;
        }

        public ForumThread(int i, Materia m, Usuario op, String t, String d)
        {
            this.Id = i;
            this.IdMateria = m.Id;
            this.IdUsuario = op.Id;
            this.Titulo = t;
            this.Descricao = d;
        }
    }
}
