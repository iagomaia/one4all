﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SQLite;
using SQLiteNetExtensions.Attributes;
using SQLite.Net.Attributes;

namespace one4all.Models
{
    public class Usuario
    {
        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }
        [MaxLength(150)]
        public String Nome { get; set; }
        public String Matricula { get; set; }
        public int Periodo { get; set; }
        [MaxLength(100)]
        public String Email { get; set; }
        [MaxLength(32)]
        public String Senha { get; set; }

        [OneToMany(CascadeOperations = CascadeOperation.All)]
        public List<ForumThread> Threads { get; set; }

        [OneToMany(CascadeOperations = CascadeOperation.All)]
        public List<Comentario> Comentarios { get; set; }

        [OneToMany(CascadeOperations = CascadeOperation.All)]
        public List<Evento> Eventos { get; set; }

        [ManyToMany(typeof(Inscricao))]
        public List<Materia> Materias{ get; set; }

        public Usuario()
        {
            Materias = new List<Materia>();
            Threads = new List<ForumThread>();
            Comentarios = new List<Comentario>();
            Eventos = new List<Evento>();
        }

        public Usuario(String nome, int periodo, String matricula, String email, String senha)
        {
            this.Nome = nome;
            this.Periodo = periodo;
            this.Email = email;
            this.Senha = senha;
            this.Matricula = matricula;
            Materias = new List<Materia>();
            Threads = new List<ForumThread>();
            Comentarios = new List<Comentario>();
            Eventos = new List<Evento>();
        }

        public Usuario(int id, String nome, int periodo, String email, String senha)
        {
            this.Id = id;
            this.Nome = nome;
            this.Periodo = periodo;
            this.Email = email;
            this.Senha = senha;
            Materias = new List<Materia>();
            Threads = new List<ForumThread>();
            Comentarios = new List<Comentario>();
            Eventos = new List<Evento>();
        }
    }
}
