﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using one4all.Models;
using System.Collections.ObjectModel;
using SQLite.Net;
using SQLiteNetExtensions.Extensions;

namespace one4all
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Principal : ContentPage
    {
        private Usuario _user;
        private SQLiteConnection _connection;
        private bool _close;

        public Principal(Usuario u)
        {
            _user = u ?? throw new ArgumentNullException();
            InitializeComponent();
        }

        protected override void OnAppearing()
        {
            _connection = DependencyService.Get<ISQLiteDb>().GetConnection();
            base.OnAppearing();
        }

        async private void ConfigBtn_Activated(object sender, EventArgs e)
        {
            await Navigation.PushModalAsync(new Config(_user));
            _user = _connection.GetWithChildren<Usuario>(_user.Id);
        }

        private async void ShowConfirmationDialog()
        {
            if (await DisplayAlert("Sair", "Deseja realmente sair?", "Sim", "Não")) _close = true;
            else _close = false;
        }

        protected override bool OnBackButtonPressed()
        {
            ShowConfirmationDialog();
            if (_close)
                return base.OnBackButtonPressed();
            else
                return true;
        }

        async private void CalendarioBtn_Clicked(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new Agenda(ref _user));
            _user = _connection.GetWithChildren<Usuario>(_user.Id);
        }

        async private void ForumBtn_Clicked(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new Forum(ref _user));
            _user = _connection.GetWithChildren<Usuario>(_user.Id);
        }

        async private void MateriasBtn_Clicked(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new Materias(ref _user));
            _user = _connection.GetWithChildren<Usuario>(_user.Id);
        }

        async private void SairBtn_Activated(object sender, EventArgs e)
        {
            await Navigation.PopAsync();
        }
    }
}