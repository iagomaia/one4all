﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using SQLite;
using one4all.Models;
using System.Collections.ObjectModel;
using SQLite.Net;
using SQLiteNetExtensions.Extensions;

namespace one4all
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MteriasModerador : ContentPage
    {
        private SQLiteConnection _connection;
        private ObservableCollection<Materia> _materias;
        private Usuario _user;
        private bool _close = true;
        public MteriasModerador(Usuario u)
        {
            _user = u;
            InitializeComponent();
        }

        async protected override void OnAppearing()
        {
            _connection = DependencyService.Get<ISQLiteDb>().GetConnection();
            _materias = new ObservableCollection<Materia>(_connection.Table<Materia>().ToList());
            MateriaList.ItemsSource = _materias;
            base.OnAppearing();
        }

        private async void ShowConfirmationDialog()
        {
            if (await DisplayAlert("Sair", "Deseja realmente sair?", "Sim", "Não")) _close = true;
            else _close = false;
        }

        protected override bool OnBackButtonPressed()
        {
            ShowConfirmationDialog();
            if (_close)   
                return base.OnBackButtonPressed();
            else
                return true;
        }

        async private void AddMateria_Activated(object sender, EventArgs e)
        {
            await Navigation.PushModalAsync(new ModeradorAddMateria());
        }

        async private void EditMateria_Activated(object sender, EventArgs e)
        {
            if (MateriaList.SelectedItem != null)
            {
                Materia m = MateriaList.SelectedItem as Materia;
                await Navigation.PushModalAsync(new ModeradorEditMateria(m));
                _connection.GetWithChildren<Materia>(m.Id);
                MateriaList.SelectedItem = null;
            }
        }

        async private void VerThread_Activated(object sender, EventArgs e)
        {
            if (MateriaList.SelectedItem != null)
            {
                Materia m = MateriaList.SelectedItem as Materia;
                await Navigation.PushAsync(new ThreadsModerador(_user, m));
                _connection.GetWithChildren<Materia>(m.Id);
                MateriaList.SelectedItem = null;
            }
        }

        async private void VerEventos_Activated(object sender, EventArgs e)
        {
            if (MateriaList.SelectedItem != null)
            {
                Materia m = MateriaList.SelectedItem as Materia;
                await Navigation.PushAsync(new EventosModerador(_user, ref m));
                MateriaList.SelectedItem = null;
            }
        }

        private void MateriaList_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            if (e.SelectedItem == null) return;
        }
    }
}