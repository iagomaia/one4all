﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using one4all.Models;

namespace one4all
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class PrincipalModerador : ContentPage
    {
        private Usuario _moderador;
        public PrincipalModerador(Usuario m)
        {
            _moderador = m;
            InitializeComponent();
        }

        async private void MateriaBtn_Clicked(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new MteriasModerador(_moderador));
        }
        
    }
}