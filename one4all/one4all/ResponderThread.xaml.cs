﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using one4all.Models;
using SQLite.Net;
using SQLiteNetExtensions.Extensions;

namespace one4all
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ResponderThread : ContentPage
    {
        private Usuario _user;
        private ForumThread _thread;
        private SQLiteConnection _connection;

        public ResponderThread(ref Usuario u, ref ForumThread thread)
        {
            _user = u ?? throw new ArgumentNullException();
            _thread = thread ?? throw new ArgumentNullException();
            InitializeComponent();
            this.Title = thread.Titulo;
        }

        protected override void OnAppearing()
        {
            _connection = DependencyService.Get<ISQLiteDb>().GetConnection();
            base.OnAppearing();
        }

        async private void VoltarBtn_Clicked(object sender, EventArgs e)
        {
            if (await DisplayAlert("Sair?", "Deseja sair sem enviar sua resposta?", "Sair", "Cancelar"))
                await Navigation.PopModalAsync();
        }

        protected override bool OnBackButtonPressed()
        {
            return true;
        }

        async private void EnviarBtn_Clicked(object sender, EventArgs e)
        {
            Comentario c = new Comentario(_user, Resposta.Text, _thread);
            try
            {
                _connection.Insert(c);
                _user.Comentarios.Add(c);
                _thread.Comentarios.Add(c);
                _connection.UpdateWithChildren(_user);
                _connection.UpdateWithChildren(_thread);
                await DisplayAlert("Resposta enviada!", "Seu comentário foi enviado com sucesso!", "OK");
                await Navigation.PopModalAsync();
            }catch (Exception ex)
            {
                await DisplayAlert("Erro", ex.ToString(), "OK");
            }
        }
    }
}