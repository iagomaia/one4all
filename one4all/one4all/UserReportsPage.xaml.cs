﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace one4all
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class UserReportsPage : ContentPage
    {
        public UserReportsPage()
        {
            InitializeComponent();
            /*ReportsList.ItemsSource = new List<Models.Report>
            {
                new Models.Report("Reported", "xxx", "vc", new Models.Evento(1, "a", "a", "d", 4)),
                new Models.Report("Reported2", "xxx", "vc", new Models.Evento(1, "a", "a", "d", 4)),
                new Models.Report("Reported3", "xxx", "vc", new Models.Evento(1, "a", "a", "d", 4))
            };*/
        }

        private void ReportsList_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            if (e.SelectedItem == null) return;

            ReportsList.SelectedItem = null;
        }
    }
}