﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using one4all.Models;
using System.Collections.ObjectModel;
using SQLite.Net;
using SQLiteNetExtensions.Extensions;

namespace one4all
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Agenda : ContentPage
    {
        private Usuario _user;
        private SQLiteConnection _connection;
        private ObservableCollection<Evento> _eventos;

        public Agenda(ref Usuario u)
        {
            _user = u ?? throw new ArgumentNullException();
            InitializeComponent();
        }

        protected override void OnAppearing()
        {
            _connection = DependencyService.Get<ISQLiteDb>().GetConnection();
            List<Evento> lista = new List<Evento>();
            foreach(Materia m in _user.Materias)
            {
                Materia mat = _connection.GetWithChildren<Materia>(m.Id);
                foreach(Evento e in mat.Eventos)
                {
                    lista.Add(e);
                }
            }
            _eventos = new ObservableCollection<Evento>(lista);
            EventList.ItemsSource = _eventos;
            base.OnAppearing();
        }

        async private void EventList_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            if (e.SelectedItem == null) return;
            var itemSelecionado = e.SelectedItem as Evento;
            await Navigation.PushAsync(new DetalheEvento(itemSelecionado));
            EventList.SelectedItem = null;
        }

        async private void AdicionarBtn_Activated(object sender, EventArgs e)
        {
            await Navigation.PushModalAsync(new AddNovoEvento(ref _user, ref _eventos));
        }
    }
}