﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using SQLite;
using one4all.Models;
using SQLite.Net;

namespace one4all
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ModeradorAddComentario : ContentPage
    {
        private Usuario _user;
        private ForumThread _thread;
        private SQLiteConnection _connection;

        public ModeradorAddComentario(Usuario u, ForumThread ft)
        {
            _user = u ?? throw new ArgumentNullException();
            _thread = ft ?? throw new ArgumentNullException();
            InitializeComponent();
        }

        protected override void OnAppearing()
        {
            _connection = DependencyService.Get<ISQLiteDb>().GetConnection();
            base.OnAppearing();
        }

        protected override bool OnBackButtonPressed()
        {
            return true;
        }

        async private void VoltarBtn_Clicked(object sender, EventArgs e)
        {
            bool voltar = await DisplayAlert("Sair?", "Deseja sair sem salvar as alterações?", "Sair", "Cancelar");

            if (voltar) await Navigation.PopModalAsync();
        }

        async private void EnviarBtn_Clicked(object sender, EventArgs e)
        {
            Comentario c = new Comentario(_user, ComentTxt.Text, _thread);
            try
            {
                _connection.Insert(c);
                await DisplayAlert("Sucesso!", "Comentário enviado com sucesso!", "OK");
                await Navigation.PopModalAsync();
            } catch (Exception ex)
            {
                await DisplayAlert("erro", ex.ToString(), "OK");
                await Navigation.PopModalAsync();
            }
        }
    }
}