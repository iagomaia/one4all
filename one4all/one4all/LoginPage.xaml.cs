﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using SQLite;
using SQLiteNetExtensions.Extensions;
using SQLiteNetExtensionsAsync.Extensions;
using one4all.Models;
using System.Collections.ObjectModel;
using SQLite.Net;

namespace one4all
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class LoginPage : ContentPage
    {
        private SQLiteConnection _connection;
        private Usuario _user;
        public LoginPage()
        {
            InitializeComponent();
        }

        async protected override void OnAppearing()
        {
            _connection = DependencyService.Get<ISQLiteDb>().GetConnection();
            try
            {
                _connection.CreateTable<Inscricao>();
                _connection.CreateTable<Usuario>();
                _connection.CreateTable<Materia>();
                _connection.CreateTable<Comentario>();
                _connection.CreateTable<ForumThread>();
                _connection.CreateTable<Evento>();

            }
            catch(Exception ex)
            {
                await DisplayAlert("Erro", ex.ToString(), "OK");
            }
            base.OnAppearing();
            
        }

        async private void CadastrarBtn_Clicked(object sender, EventArgs e)
        {
            await Navigation.PushModalAsync(new FormCadastro());
        }

        async private void LoginBtn_Clicked(object sender, EventArgs e)
        {
            try
            {
                List<Usuario> users = _connection.Table<Usuario>().Where(u => u.Email.ToUpper() == EmailEntry.Text.ToUpper()).ToList();
                _user = users[0];
                if (_user.Senha.Equals(senha.Text) && users != null)
                {
                    _user = _connection.GetWithChildren<Usuario>(_user.Id);
                    EmailEntry.Text = "";
                    senha.Text = "";
                    if (_user.Email.ToLower().Equals("moderador@moderador.com"))
                        await Navigation.PushAsync(new MteriasModerador(_user));
                    else
                        await Navigation.PushAsync(new Principal(_user));
                }
                else
                {
                    await DisplayAlert("Erro", "Usuário ou senha inválidos", "OK");
                }
            }
            catch(Exception ex)
            {
                await DisplayAlert("Erro", "Erro ao fazer login, verifique os dados!", "OK");
            }
            
            
        }
    }
}