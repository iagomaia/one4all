﻿using SQLite;
using SQLite.Net;

namespace one4all
{
    public interface ISQLiteDb
    {
        SQLiteConnection GetConnection();
    }
}

