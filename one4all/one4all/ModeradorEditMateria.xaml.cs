﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using SQLite;
using one4all.Models;
using SQLite.Net;

namespace one4all
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ModeradorEditMateria : ContentPage
    {
        private Materia _materia;
        private SQLiteConnection _connection;
        public ModeradorEditMateria(Materia m)
        {
            _materia = m  ?? throw new ArgumentNullException();
            InitializeComponent();
            BindingContext = _materia;
        }

        protected override void OnAppearing()
        {
            _connection = DependencyService.Get<ISQLiteDb>().GetConnection();
            base.OnAppearing();
        }

        protected override bool OnBackButtonPressed()
        {
            return true;
        }

        async private void VoltarBtn_Clicked(object sender, EventArgs e)
        {
            bool voltar = await DisplayAlert("Sair?", "Deseja sair sem salvar as alterações?", "Sair", "Cancelar");
            if (voltar) await Navigation.PopModalAsync();
        }

        async private void SalvarBtn_Clicked(object sender, EventArgs e)
        {
            Materia m = _materia;
            m.Periodo = Convert.ToInt32(periodo.Text);
            m.Nome = nome.Text;
            m.Professor = professor.Text;
            try
            {
                _connection.InsertOrReplace(m);
                await DisplayAlert(m.Nome, "Matéria alterada com sucesso", "Sair");
                await Navigation.PopModalAsync();

            }
            catch (Exception ex)
            {
                await DisplayAlert("Erro", ex.ToString(), "OK");
            }
        }

        async private void ExcluirBtn_Activated(object sender, EventArgs e)
        {
            bool flag = await DisplayAlert("Excluir?", "Deseja excluir a matéria selecionada?", "Sim", "Não");
            if (flag)
            {
                try
                {
                    _connection.Delete(_materia);
                    await DisplayAlert("Excluido!", "Item excluido com sucesso!", "OK");
                    await Navigation.PopModalAsync();
                } catch(Exception ex)
                {
                    await DisplayAlert("Erro", ex.ToString(), "OK");
                }
            }
        }
    }
}