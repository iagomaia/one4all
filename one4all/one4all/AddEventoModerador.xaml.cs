﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using one4all.Models;
using SQLite.Net;
using SQLiteNetExtensions.Extensions;

namespace one4all
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class AddEventoModerador : ContentPage
    {
        private SQLiteConnection _connection;
        private Usuario _user;
        private Materia _materia;
        private bool _flag;
        public AddEventoModerador(Usuario u, Materia m)
        {
            _user = u ?? throw new ArgumentNullException();
            _materia = m ?? throw new ArgumentNullException();
            InitializeComponent();
        }

        protected override void OnAppearing()
        {
            _connection = DependencyService.Get<ISQLiteDb>().GetConnection();
            base.OnAppearing();
        }
        protected override bool OnBackButtonPressed()
        {
            return true;
        }

        async private void VoltarBtn_Clicked(object sender, EventArgs e)
        {
            bool voltar = await DisplayAlert("Sair?", "Deseja sair sem salvar as alterações?", "Sair", "Cancelar");

            if (voltar) await Navigation.PopModalAsync();
        }

        async private Task VerificaNome()
        {
            if (NomeEvento.Text == null)
            {
                _flag = false;
                await DisplayAlert("Erro", "O campo nome deve estar preenchido!", "OK");
            }
        }

        async private Task VerificaNota()
        {
            if (ValorEvento.Text == null)
            {
                _flag = false;
                await DisplayAlert("Erro", "O campo valor deve estar preenchido!", "OK");
            }
        }

        async private Task VerificaDesc()
        {
            if (DescEvento.Text == null)
            {
                _flag = false;
                await DisplayAlert("Erro", "O campo de descrição deve estar preenchido", "OK");
            }
        }

        async private void EnviarBtn_Clicked(object sender, EventArgs e)
        {
            _flag = true;
            await VerificaNome();
            await VerificaDesc();
            await VerificaNota();
            if (_flag)
            {
                Evento ev = new Evento(
                new DateTime(DataEvento.Date.Year, DataEvento.Date.Month, DataEvento.Date.Day),
                NomeEvento.Text, DescEvento.Text, Convert.ToInt32(ValorEvento.Text),
                _materia, _user);

                try
                {
                    _connection.Insert(ev);
                    _materia.Eventos.Add(ev);
                    _connection.UpdateWithChildren(_materia);
                    if (await DisplayAlert("Evento Cadastrado com sucesso!", "Deseja sair ou cadastrar outro?", "Sair", "Cadastrar Novo"))
                    {
                        await Navigation.PopModalAsync();
                    }
                    else
                    {
                        DataEvento.Date = DateTime.Today;
                        NomeEvento.Text = "";
                        DescEvento.Text = "";
                        ValorEvento.Text = "";
                    }

                }
                catch (Exception ex)
                {
                    await DisplayAlert("Erro", ex.ToString(), "OK");
                }
            }
            

        }
    }
}