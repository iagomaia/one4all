﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using one4all.Models;
using System.Collections.ObjectModel;
using SQLite.Net;
using SQLiteNetExtensions.Extensions;

namespace one4all
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ThreadsForum : ContentPage
    {
        private Materia _materia;
        private Usuario _user;
        private ObservableCollection<ForumThread> _threads;
        private SQLiteConnection _connection;

        public ThreadsForum(ref Usuario u, Materia m)
        {
            _materia = m ?? throw new ArgumentNullException();
            _user = u ?? throw new ArgumentNullException();
            InitializeComponent();
            this.Title = m.Nome;
        }

        protected override void OnAppearing()
        {
            _connection = DependencyService.Get<ISQLiteDb>().GetConnection();
            _materia = _connection.GetWithChildren<Materia>(_materia.Id);
            _threads = new ObservableCollection<ForumThread>(_materia.Threads);
            ThreadsList.ItemsSource = _threads;
            base.OnAppearing();
        }

        async private void ThreadsList_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            if (e.SelectedItem == null) return;
            var item = e.SelectedItem as ForumThread;
            await Navigation.PushAsync(new DetalhesThread(ref _user, item));
            ThreadsList.SelectedItem = null;
        }

        async private void AddThread_Activated(object sender, EventArgs e)
        {
            await Navigation.PushModalAsync(new AddThreadForm(ref _materia, ref _user));
        }
    }
}