﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using one4all.Models;
using System.Collections.ObjectModel;
using SQLite.Net;
using SQLiteNetExtensions.Extensions;

namespace one4all
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class DetalhesThread : ContentPage
    {
        private ForumThread _thread;
        private Usuario _user;
        private ObservableCollection<Comentario> _comentarios;
        private SQLiteConnection _connection;

        public DetalhesThread(ref Usuario u, ForumThread t)
        {
            _user = u ?? throw new ArgumentNullException();
            _thread = t ?? throw new ArgumentNullException();
            InitializeComponent();
            this.Title = _thread.Titulo;
        }

        protected override void OnAppearing()
        {
            _connection = DependencyService.Get<ISQLiteDb>().GetConnection();
            _thread = _connection.GetWithChildren<ForumThread>(_thread.Id);
            _comentarios = new ObservableCollection<Comentario>(_thread.Comentarios);
            ComentsList.ItemsSource = _comentarios;
            base.OnAppearing();
        }

        async private void AddComent_Activated(object sender, EventArgs e)
        {
            await Navigation.PushModalAsync(new ResponderThread(ref _user, ref _thread));
        }
        

        private void ComentsList_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            if (e.SelectedItem == null) return;
            ComentsList.SelectedItem = null;
        }
    }
}