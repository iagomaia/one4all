﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using SQLite;
using one4all.Models;
using SQLite.Net;

namespace one4all
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ModeradorEditThread : ContentPage
    {
        private SQLiteConnection _connection;
        private ForumThread _thread;

        public ModeradorEditThread(ForumThread ft)
        {
            _thread = ft ?? throw new ArgumentNullException();
            BindingContext = _thread;
            InitializeComponent();
        }

        protected override void OnAppearing()
        {
            _connection = DependencyService.Get<ISQLiteDb>().GetConnection();
            base.OnAppearing();
        }

        protected override bool OnBackButtonPressed()
        {
            return true;
        }

        async private void VoltarBtn_Clicked(object sender, EventArgs e)
        {
            bool voltar = await DisplayAlert("Sair?", "Deseja sair sem salvar as alterações?", "Sair", "Cancelar");
            if (voltar) await Navigation.PopModalAsync();
        }

        async private void SalvarBtn_Clicked(object sender, EventArgs e)
        {
            ForumThread ft = _thread;
            ft.Titulo = TituloThread.Text;
            ft.Descricao = DescThread.Text;

            try
            {
                _connection.InsertOrReplace(ft);
                await DisplayAlert(ft.Titulo, "Alterações salvas com sucesso!", "OK");

            } catch (Exception ex)
            {
                await DisplayAlert("Erro", ex.ToString(), "OK");
            }
            await Navigation.PopModalAsync();
        }

        async private void ExcluirBtn_Clicked(object sender, EventArgs e)
        {
            try
            {
                _connection.Delete(_thread);
                await DisplayAlert("Item Excluido", "Item excluido com sucesso!", "OK");
                await Navigation.PopModalAsync();
            }catch(Exception ex)
            {
                await DisplayAlert("Erro", ex.ToString(), "OK");
            }
            await Navigation.PopModalAsync();
        }
    }
}