﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using one4all.Models;
using SQLite;
using SQLite.Net;

namespace one4all
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ModeradorAddThread : ContentPage
    {
        private Materia _materia;
        private Usuario _user;
        private SQLiteConnection _connection;
        public ModeradorAddThread(Materia m, Usuario u)
        {
            _user = u;
            _materia = m;
            InitializeComponent();
        }

        protected override void OnAppearing()
        {
            _connection = DependencyService.Get<ISQLiteDb>().GetConnection();
            base.OnAppearing();
        }

        protected override bool OnBackButtonPressed()
        {
            return true;
        }

        async private void VoltarBtn_Clicked(object sender, EventArgs e)
        {
            bool voltar = await DisplayAlert("Sair?", "Deseja sair sem salvar as alterações?", "Sair", "Cancelar");

            if (voltar) await Navigation.PopModalAsync();
        }

        async private void AdicionarBtn_Clicked(object sender, EventArgs e)
        {
            ForumThread ft = new ForumThread(_materia, _user, TituloThread.Text, DescThread.Text);
            try
            {
                _connection.Insert(ft);
                if (await DisplayAlert(ft.Titulo, "Cadastro realizado com sucesso!", "Sair", "Add outra"))
                    await Navigation.PopModalAsync();
                else
                {
                    TituloThread.Text = "";
                    DescThread.Text = "";
                }

            }
            catch (Exception ex)
            {
                await DisplayAlert("Erro", ex.ToString(), "OK");
            }
        }
    }
}